# secure-boot-sig-modules

Signitured modules for your system.

* [Ubuntu Secure Boot](https://ubuntu.com/blog/how-to-sign-things-for-secure-boot)
* [VirtualBox Kernel Modules](https://gist.github.com/reillysiemens/ac6bea1e6c7684d62f544bd79b2182a4)
* [CentOS Secure Boot](https://fedoraproject.org/wiki/Secureboot)
* [UEFI_Secure_Boot_Guide](https://docs.fedoraproject.org/en-US/Fedora/18/html/UEFI_Secure_Boot_Guide/index.html)
* [Debian Wiki](https://wiki.debian.org/SecureBoot)
