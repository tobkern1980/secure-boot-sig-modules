#!/bin/bash -e
#
  if [ `id -u` != 0 ]; then
    echo "Das Script kann nur als Benutzer Root Ausgefuehrt werden!"
    sleep 2
    exit 1
  fi

name="$(getent passwd $(whoami) | awk -F: '{print $1}')"

if [ ! -d ./module-signing/ ]
 then
    mkdir module-signing
fi

out_dir= "./module-signing/"
#sign-file="/usr/src/kernels/$(uname -r)/scripts/sign-file"
#VBOX_MODULES="/usr/lib/modules/5.3.0-46-generic"
# run-openssl

openssl req -config ./openssl.cnf \
  -new -x509 -newkey rsa:2048 \
  -nodes -days 36500 -outform DER \
  -keyout "./module-signing/MOK.priv" \
  -out "./module-signing/MOK.der"
#  -subj "/CN=$(hostnamectl --static)/"

sudo chmod 600 ./module-signing/MOK.*

# enroll-key
sudo mokutil --import ./module-signing/MOK.der

apt -y install shim shim-signed

update-grub2

# bc check
command -v bc >/dev/null 2>&1 || { echo >&2 "I require bc but it's not installed.  Aborting."; exit 1; }

# sign-modules

# ls -1 /usr/lib/modules/$(uname -r)/updates/dkms/*.ko

KERNEL_MODULES=(
  /usr/lib/modules/$(uname -r)/updates/dkms/vboxdrv.ko
  /usr/lib/modules/$(uname -r)/updates/dkms/vboxnetadp.ko
  /usr/lib/modules/$(uname -r)/updates/dkms/vboxnetflt.ko
  /usr/lib/modules/$(uname -r)/updates/dkms/vboxpci.ko
)
# https://unix.stackexchange.com/questions/385198/get-all-files-but-the-files-in-array-bash
# https://delightlylinux.wordpress.com/2017/04/08/put-filenames-in-bash-array/

# add wireguars to arrey if

  #/usr/lib/modules/$(uname -r)/updates/dkms/wireguard.ko

echo "Installing the folloing Modules $( printf '%s\n' ${KERNEL_MODULES[@]})"

for module in ${KERNEL_MODULES[@]}
  do
    sudo kmodsign sha512 ./module-signing//MOK.priv ./module-signing/MOK.der $module
    echo "Sigin Module $module"
  done
