#!/usr/bin/env bash
#
#
# Script-Name		  : myscriptz.sh
# Version	        : 0.0.1
# Autor		        : Tobias Kern
# Datum		        : $(date)
# Lizenz	        : GPLv3
# Depends     		:
#
# set -x
########################################################################
#### Bach functions
#########################################################################
#
# ------------------------------------------------------
# If a script needs sudo access, call this function which
# requests sudo access and then keeps it alive.
# ------------------------------------------------------
function needSudo() {
  # Update existing sudo time stamp if set, otherwise do nothing.
  sudo -v
  while true
    do sudo -n true
    sleep 60
    kill -0 "$$" || exit
  done 2>/dev/null &
}

# File Checks
# ------------------------------------------------------
# A series of functions which make checks against the filesystem. For
# use in if/then statements.
function is_dir() {
  if [[ -d "$1" ]]; then
    return 0
  fi
  return 1
}

function is_file() {
  if [[ -f "$1" ]]; then
    return 0
  fi
  return 1
}

trim() {
  # Removes all leading/trailing whitespace
  # Usage examples:
  #     echo "  foo  bar baz " | trim  #==> "foo  bar baz"
  ltrim "$1" | rtrim "$1"
}

# TIMESTAMPS
# ------------------------------------------------------
# Prints the current date and time in a variety of formats:
#
# ------------------------------------------------------
now=$(LC_ALL=C date +"%m-%d-%Y %r")                     # Returns: 06-14-2015 10:34:40 PM
datestamp=$(LC_ALL=C date +%Y-%m-%d)                    # Returns: 2015-06-14
hourstamp=$(LC_ALL=C date +%r)                          # Returns: 10:34:40 PM
timestamp=$(LC_ALL=C date +%Y%m%d_%H%M%S)               # Returns: 20150614_223440
today=$(LC_ALL=C date +"%m-%d-%Y")                      # Returns: 06-14-2015
longdate=$(LC_ALL=C date +"%a, %d %b %Y %H:%M:%S %z")   # Returns: Sun, 10 Jan 2016 20:47:53 -0500
gmtdate=$(LC_ALL=C date -u -R | sed 's/\+0000/GMT/')    # Returns: Wed, 13 Jan 2016 15:55:29 GMT

name="$(getent passwd $(whoami) | awk -F: '{print $1}')"

if [ ! -d ./module-signing/ ]
 then
    mkdir module-signing
fi

###########################################################################################################
###########                   Colors in shell                                        ######################
###########################################################################################################
black() { echo "$(tput setaf 0)$*$(tput setaf 9)"; }
red() { echo "$(tput setaf 1)$*$(tput setaf 9)"; }
green() { echo "$(tput setaf 2)$*$(tput setaf 9)"; }
yellow() { echo "$(tput setaf 3)$*$(tput setaf 9)"; }
blue() { echo "$(tput setaf 4)$*$(tput setaf 9)"; }
magenta() { echo "$(tput setaf 5)$*$(tput setaf 9)"; }
cyan() { echo "$(tput setaf 6)$*$(tput setaf 9)"; }
white() { echo "$(tput setaf 7)$*$(tput setaf 9)"; }

##############################################################################################################
#######                       Vars                                                        ####################
##############################################################################################################

out_dir= "$(pwd)/module-signing/"

sign-file="/usr/src/kernels/$(uname -r)/scripts/sign-file"

#VBOX_MODULES="/usr/lib/modules/5.3.0-46-generic"

#  New cert and key to sign Kernel modules
#openssl req -config ./openssl.cnf \
#  -new -x509 -newkey rsa:2048 \
#  -nodes -days 36500 -outform DER \
#  -keyout "./module-signing/MOK.priv" \
#  -out "./module-signing/MOK.der"
#  -subj "/CN=$(hostnamectl --static)/"

sudo chmod 600 ./module-signing/MOK.*

# enroll-key
#sudo mokutil --import ./module-signing/MOK.der

#apt -y reinstall shim shim-signed

#update-grub2

# bc check
command -v bc >/dev/null 2>&1 || { echo >&2 "I require bc but it's not installed.  Aborting."; exit 1; }

# sign-modules

ls -1 "/usr/lib/modules/$(uname -r)/updates/dkms/*.ko"

KERNEL_MODULES=(
  ls -1 "/usr/lib/modules/$(uname -r)/updates/dkms/" | grep .ko
)
# https://unix.stackexchange.com/questions/385198/get-all-files-but-the-files-in-array-bash
# https://delightlylinux.wordpress.com/2017/04/08/put-filenames-in-bash-array/

# add wireguars to arrey if

  #echo :--  /usr/lib/modules/$(uname -r)/updates/dkms/wireguard.ko

echo "Installing the folloing Modules $( printf '%s\n' ${KERNEL_MODULES[@]})"

for module in ${KERNEL_MODULES[@]}
  do
    sudo kmodsign sha512 ./module-signing/MOK.priv ./module-signing/MOK.der $module
    echo "Sign: $module Kernel Module"
  done

exit 0
