#!/bin/bash -e
#
  if [ `id -u` != 0 ]; then
    echo "Das Script kann nur als Benutzer Root Ausgefuehrt werden!"
    sleep 2
    exit 1
  fi

name="$(getent passwd $(whoami) | awk -F: '{print $1}')"

if [ ! -d ./module-signing/ ]
 then
    mkdir module-signing
fi

command -v bc >/dev/null 2>&1 || { echo >&2 "I require bc but it's not installed.  Aborting."; exit 1; }

ls -1 /usr/lib/modules/$(uname -r)/updates/dkms/

KERNEL_MODULES=(
  $(ls -1 /usr/lib/modules/$(uname -r)/updates/dkms/ | grep .ko)
)


for module in ${KERNEL_MODULES[@]}
  do
    sudo kmodsign sha512 ./module-signing//MOK.priv ./module-signing/MOK.der $module
    echo "Sigin Module $module"
  done
