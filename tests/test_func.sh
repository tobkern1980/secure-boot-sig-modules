#!/usr/bin/env bash
#
#
# Script-Name		  : myscriptz.sh
# Version	        : 0.0.1
# Autor		        : Tobias Kern
# Datum		        : $(date)
# Lizenz	        : GPLv3
# Depends     		:
#
set -x
########################################################################
#### Bach functions
#########################################################################
#
# ------------------------------------------------------
# If a script needs sudo access, call this function which
# requests sudo access and then keeps it alive.
# ------------------------------------------------------
function needSudo() {
  # Update existing sudo time stamp if set, otherwise do nothing.
  sudo -v
  while true
    do sudo -n true
    sleep 60
    kill -0 "$$" || exit
  done 2>/dev/null &
}

# File Checks
# ------------------------------------------------------
# A series of functions which make checks against the filesystem. For
# use in if/then statements.
function is_dir() {
  if [[ -d "$1" ]]; then
    return 0
  fi
  return 1
}

function is_file() {
  if [[ -f "$1" ]]; then
    return 0
  fi
  return 1
}

trim() {
  # Removes all leading/trailing whitespace
  # Usage examples:
  #     echo "  foo  bar baz " | trim  #==> "foo  bar baz"
  ltrim "$1" | rtrim "$1"
}

name="$(getent passwd $(whoami) | awk -F: '{print $1}')"

if [ ! -d ./module-signing/ ]
 then
    mkdir module-signing
    echo add dir module-signing
fi

###########################################################################################################
###########                   Colors in shell                                        ######################
###########################################################################################################
black() { echo "$(tput setaf 0)$*$(tput setaf 9)"; }
red() { echo "$(tput setaf 1)$*$(tput setaf 9)"; }
green() { echo "$(tput setaf 2)$*$(tput setaf 9)"; }
yellow() { echo "$(tput setaf 3)$*$(tput setaf 9)"; }
blue() { echo "$(tput setaf 4)$*$(tput setaf 9)"; }
magenta() { echo "$(tput setaf 5)$*$(tput setaf 9)"; }
cyan() { echo "$(tput setaf 6)$*$(tput setaf 9)"; }
white() { echo "$(tput setaf 7)$*$(tput setaf 9)"; }

##############################################################################################################
#######                       Vars                                                        ####################
##############################################################################################################

out_dir= "$(pwd)/module-signing/"

echo $out_dir

#if [[ ! -f ./module-signing/MOK.priv && ./module-signing/MOK.der ]]
#  then
#  New cert and key to sign Kernel modules
#openssl req -config ./openssl.cnf \
#  -new -x509 -newkey rsa:2048 \
#  -nodes -days 36500 -outform DER \
#  -keyout "./module-signing/MOK.priv" \
#  -out "./module-signing/MOK.der"
#  -subj "/CN=$(hostnamectl --static)/"
#  else
#  echo "MOK files are present"
#fi

command -v bc >/dev/null 2>&1 || { echo >&2 "I require bc but it's not installed.  Aborting."; exit 1; }

ls -1 /usr/lib/modules/$(uname -r)/updates/dkms/

KERNEL_MODULES=(
  $(ls -1 /usr/lib/modules/$(uname -r)/updates/dkms/ | grep .ko)
)

# echo "Installing the folloing Modules $( printf '%s\n' ${KERNEL_MODULES[@]} )"